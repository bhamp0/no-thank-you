#!/usr/bin/env bash
set -euo pipefail
shopt -s globstar nullglob

TRANSLATIONS=()
LANGUAGES=()
function join_by { local d=$1; shift; echo -n "$1"; shift; printf "%s" "${@/#/$d}"; }

## Building each language index.html
for translationFile in translations/*.env; do
  LANGUAGE=$(basename "$translationFile" ".env")
  # shellcheck source=/dev/null
  source "$translationFile"
  mkdir -p "public/$LANGUAGE"
  sed -e "s/{LANGUAGE}/$LANGUAGE/g" "./template.html" |
  sed -e "s/{TITLE}/$TITLE/g" |
  sed -e "s/{ADDRESS}/$ADDRESS/g" |
  sed -e "s/{NOT_LOOKING}/$NOT_LOOKING/g" |
  sed -e "s/{HAPPY}/$HAPPY/g" |
  sed -e "s/{NO_SOLICITING}/$NO_SOLICITING/g" |
  sed -e "s/{NOONE_ELSE}/$NOONE_ELSE/g" > "public/$LANGUAGE/index.template"

  TRANSLATIONS+=("<a href='../${LANGUAGE}/'>${NAME}</a>")
  LANGUAGES+=("'${LANGUAGE}'")
done


## Add links to all languages
TRANSLATIONS_JOINED=$(join_by " \\&middot; " "${TRANSLATIONS[@]}" )
for template in public/*/*.template; do
  sed -e "s#{TRANSLATIONS}#$TRANSLATIONS_JOINED#g" "$template" > "${template/template/html}"
  rm -f "$template"
done

## Add user language redirect to main page via JS
LANGUAGES_JOINED=$(join_by ", " "${LANGUAGES[@]}" )
sed -e "s#\\.\\./#./#g" "public/en/index.html" |
  sed -e"s#</script>#var l=(navigator.language||navigator.userLanguage).substr(0,2);[$LANGUAGES_JOINED].indexOf(l)>=0\\&\\&(location.href=location.href+'/'+l);</script>#" > "public/index.html"

## gzip all files in public
find public -iname "*.gz" -print0 | xargs -0 rm -f
find public -type f -print0 | xargs -0 gzip -f -k
